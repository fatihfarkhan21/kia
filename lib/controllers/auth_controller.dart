import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:kia/models/user_model.dart' as AppModel;

class AuthController extends GetxController {
  FirebaseAuth _auth = FirebaseAuth.instance;
  RxBool isAuth = false.obs;
  Stream<AppModel.User> authStream;
  bool _isNewUser = false;
  AppModel.User _newUserProfile;
  Rx<AppModel.User> userProfile = AppModel.User().obs;

  CollectionReference userProfileCR =
      FirebaseFirestore.instance.collection("users");

  @override
  void onInit() {
    print("cek");
    super.onInit();

    authStream = _auth.authStateChanges().asyncMap(getUserProfile);
    print("cek");
    authStream.listen((_user) {
      print("cek");
      if (_user == null) {
        isAuth.value = false;
      } else {
        isAuth.value = true;
        userProfile.value = _user;
        print("cek");
        Get.snackbar("Welcome", "Hello Brads ${_user.userName}",
            snackPosition: SnackPosition.BOTTOM);
      }
    });
  }

  Future<bool> login({String email, String password}) async {
    bool result = false;

    try {
      await signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      Get.snackbar(
        "Error Login",
        e.message,
        snackPosition: SnackPosition.BOTTOM,
      );
    }

    return result;
  }

  Future<bool> signUp({String userName, String email, String password}) async {
    bool result = false;

    try {
      await createUserWithEmailAndPassword(
          userName: userName, userEmail: email, userPassword: password);
    } catch (e) {
      Get.snackbar(
        "Error New User",
        e.message,
        snackPosition: SnackPosition.BOTTOM,
      );
    }

    print("cek");
    return result;
  }

  Future<AppModel.User> getUserProfile(User user) async {
    AppModel.User userProfile;

    if (user == null) return userProfile;

    try {
      if (_isNewUser) {
        userProfile = _newUserProfile;
      } else {
        DocumentSnapshot userDs = await userProfileCR.doc(user.uid).get();

        if (userDs.exists) {
          userProfile = AppModel.User.fromMap(userDs.data());
        }
      }
    } catch (e) {
      rethrow;
    }

    return userProfile;
  }

  Future<void> signInWithEmailAndPassword(
      {@required String email, @required String password}) async {
    try {
      await _auth.signInWithEmailAndPassword(
          email: email.trim(), password: password);
    } catch (e) {
      rethrow;
    }
  }

  Future<void> createUserWithEmailAndPassword(
      {@required String userName,
      @required String userEmail,
      @required String userPassword}) async {
    try {
      _isNewUser = true;
      print("cek");
      AppModel.User newUser = AppModel.User(
          userEmail: userEmail, userName: userName, userStatus: 'ACTIVE');

      _newUserProfile = newUser;

      UserCredential userCredential =
          await _auth.createUserWithEmailAndPassword(
              email: userEmail, password: userPassword);
      await _auth.currentUser.updateProfile(displayName: userName);

      newUser = newUser.copyWith(userId: userCredential.user.uid);

      userProfileCR.doc(userCredential.user.uid).set(newUser.toMap());

      print("cek");
    } catch (e) {
      rethrow;
    }

    return true;
  }

  Future<bool> logOut() async {
    bool result = false;
    await signOut();
    try {
      Get.snackbar(
        "Bye bye",
        "Have a nice day",
        snackPosition: SnackPosition.BOTTOM,
      );
    } catch (e) {
      Get.snackbar(
        "Error logout",
        e.message,
        snackPosition: SnackPosition.BOTTOM,
      );
    }
    return result;
  }

  Future<void> signOut() async {
    try {
      await _auth.signOut();
    } catch (e) {
      rethrow;
    }
  }

  Future<AppModel.User> getCurrentUser() async {
    AppModel.User userProfile;
    User user = _auth.currentUser;
    try {
      DocumentSnapshot userDS = await userProfileCR.doc(user.uid).get();
      if (userDS.exists) {
        userProfile = AppModel.User.fromMap(userDS.data());
      }
    } catch (e) {}
    return userProfile;
  }
}
