import 'package:get/get.dart';
import 'package:kia/controllers/app_controller.dart';
import 'package:kia/controllers/auth_controller.dart';

class AppBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<AuthController>(AuthController(), permanent: true);
    Get.put<AppController>(AppController(), permanent: true);
    print("cek");
  }
}
