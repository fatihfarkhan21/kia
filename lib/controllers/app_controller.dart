import 'package:get/get.dart';
import 'package:kia/controllers/auth_controller.dart';
import 'package:kia/dev/auth_dev_page.dart';
import 'package:kia/dev/home_dev.dart';
import 'package:kia/models/app_model.dart';

class AppController extends GetxController {
  Rx<App> app = App(isAuth: false, isLoading: true, appName: 'KIA').obs;

  @override
  void onInit() {
    super.onInit();
    Get.find<AuthController>().isAuth.listen((isAuth) {
      app(app.value.copyWith(isAuth: isAuth));

      print("cek");
    });
    appInit();
  }

  Future<void> appInit() async {
    print("App Init ======");
    await Future.delayed(Duration(seconds: 5));

    ever(app, goToLandingPage);

    app(app.value.copyWith(isLoading: false));
  }

  goToLandingPage(App appStatus) {
    if (appStatus.isAuth) {

      Get.offAll(HomeDev());
      print("isAuth true");
    } else {
      Get.offAll(AuthDevPage());
      print("isAuth false");
    }
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
