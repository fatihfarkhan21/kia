import 'package:get/get.dart';
import 'package:kia/dev/auth_dev_page.dart';
import 'package:kia/dev/dev.dart';
import 'package:kia/dev/splash_dev.dart';

class AppRoutes {
  AppRoutes._();
  static final routes = [
    GetPage(name: "/", page: () => SplashDev()),
    GetPage(name: "auth", page: () => AuthDevPage()),
    GetPage(name: "home", page: () => HomeDev())
  ];
}