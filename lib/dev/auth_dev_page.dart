import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kia/controllers/auth_controller.dart';

class AuthDevPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dev"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(onPressed: () {
                 Get.find<AuthController>().login(
                   email: "myemail@gmail.com",
                   password: "12345678"
                 );
              }, child: Text("Login")),
              ElevatedButton(
                  onPressed: () {
                    Get.find<AuthController>().signUp(
                        userName: "Farkhan",
                        email: "fatih@gmail.com",
                        password: "12345678");
                  },
                  child: Text("Sign Up")),
              ElevatedButton(onPressed: () {
                Get.find<AuthController>().logOut();
              }, child: Text("LogOut"))
            ],
          )
        ],
      ),
    );
  }
}
