import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kia/controllers/auth_controller.dart';

class HomeDev extends StatelessWidget {

  final AuthController authController = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        actions: [
          IconButton(icon: Icon(Icons.exit_to_app), onPressed: (){
            Get.find<AuthController>().logOut();
          })
        ],
      ),
      body: Container(
        child: Center(
          child: Text("${authController.userProfile.value.userEmail}"),
        ),
      ),
    );
  }
}