import 'package:flutter/material.dart';


ThemeData appTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: pregnancyColorSwatch,
    primaryColor: pregnancyColorSwatch,
    accentColor: Colors.white,
    iconTheme: primaryIconTheme,
    primaryIconTheme: primaryIconTheme,
    accentIconTheme: accentIconTheme,
    appBarTheme: appBarTheme,
    tabBarTheme: tabBarTheme,
    fontFamily: 'Roboto',
    textTheme: textTheme,
    buttonColor: pregnancyColorScondary[500],
    buttonTheme: buttonThemeData,
    scaffoldBackgroundColor: Color(0xFFF8FDFE),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    floatingActionButtonTheme: floatingActionButtonTheme,
    inputDecorationTheme: inputDecorationTheme());

const Color color01 = Colors.black;
const Color color02 = Color.fromARGB(0xFF, 0x37, 0xC8, 0xE7);
const Color color03 = Color.fromARGB(0xFF, 0xF0, 0x62, 0x92);
const Color color04 = Color.fromARGB(0xFF, 0xEC, 0x40, 0x7A);
const Color color05 = Color.fromARGB(0xFF, 0x04, 0x64, 0x79);
const Color color06 = Color.fromARGB(0xFF, 0x16, 0xA7, 0xD7);

const MaterialColor pregnancyColorSwatch =
    MaterialColor(0xFF65DEF1, pregnancyColor);
const Map<int, Color> pregnancyColor = {
  50: Color(0xFFEDFBFD),
  100: Color(0xFFD1F5FB),
  200: Color(0xFFB2EFF8),
  300: Color(0xFF93E8F5),
  400: Color(0xFF7CE3F3),
  500: Color(0xFF65DEF1),
  600: Color(0xFF5DDAEF),
  700: Color(0xFF53D5ED),
  800: Color(0xFF49D1EB),
  900: Color(0xFF37C8E7),
};

const Map<int, Color> pregnancyColorScondary = {
  50: Color(0xFFFCEAEC),
  100: Color(0xFFF8BBD0),
  200: Color(0xFFF48FB1),
  300: Color(0xFFF06292),
  400: Color(0xFFEC407A),
  500: Color(0xFFE91E63),
  600: Color(0xFFD81B60),
  700: Color(0xFFC2185B),
  800: Color(0xFFAD1457),
  900: Color(0xFF880E4F),
};

const IconThemeData primaryIconTheme =
    IconThemeData(color: color01, opacity: 1.0);
const IconThemeData accentIconTheme =
    IconThemeData(color: color01, opacity: 1.0);

const FloatingActionButtonThemeData floatingActionButtonTheme =
    FloatingActionButtonThemeData(
  foregroundColor: Color(0xFFF17F29),
);

const ButtonThemeData buttonThemeData = ButtonThemeData(
  // buttonColor: Color.fromARGB(0xFF, 0x48, 0xFE, 0xEC),
  textTheme: ButtonTextTheme.primary,
);

const AppBarTheme appBarTheme =
    AppBarTheme(elevation: 0, color: Color(0xFF65DEF1));
const TabBarTheme tabBarTheme = TabBarTheme(
    labelColor: color01,
    unselectedLabelColor: Color.fromARGB(0x65, 0x00, 0x14, 0x12),
    labelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
    unselectedLabelStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w500));

InputDecorationTheme inputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(5),
    borderSide: BorderSide(color: color02),
    gapPadding: 10,
  );
  return InputDecorationTheme(
    contentPadding: EdgeInsets.symmetric(horizontal: 42, vertical: 20),
    enabledBorder: outlineInputBorder,
    focusedBorder: outlineInputBorder,
    border: outlineInputBorder,
  );
}

const TextTheme textTheme = TextTheme(
  headline1: TextStyle(color: color01),
  headline2: TextStyle(color: color01),
  headline3: TextStyle(color: color01),
  headline4: TextStyle(color: color01),
  headline5: TextStyle(
      color: color01,
      fontSize: 25,
      fontWeight: FontWeight.bold,
      fontFamily: "Roboto"),
  headline6: TextStyle(
      color: color01,
      fontSize: 20,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto"),
  subtitle1: TextStyle(
      color: color01,
      fontSize: 15,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto"),
  subtitle2: TextStyle(
      color: color02,
      fontSize: 14,
      fontWeight: FontWeight.w300,
      fontFamily: "Roboto"),
  //for all Default Text
  bodyText1: TextStyle(
      color: color01,
      fontSize: 12,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto"),
  bodyText2: TextStyle(
      color: color01,
      fontSize: 12,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto"),
);