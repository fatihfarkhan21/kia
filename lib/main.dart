import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kia/app_theme.dart';

import 'controllers/app_bindings.dart';
import 'dev/auth_dev_page.dart';
import 'routes/app_routes.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      initialBinding: AppBinding(),
      theme: appTheme,
      initialRoute: "/",
      getPages: AppRoutes.routes,
    );
  }
}
