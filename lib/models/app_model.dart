import 'dart:convert';

import 'package:equatable/equatable.dart';

class App extends Equatable {
  final bool isLoading;
  final bool isAuth;
  final String appName;
  App({
     this.isLoading,
     this.isAuth,
     this.appName,
  });
  

  App copyWith({
    bool isLoading,
    bool isAuth,
    String appName,
  }) {
    return App(
      isLoading: isLoading ?? this.isLoading,
      isAuth: isAuth ?? this.isAuth,
      appName: appName ?? this.appName,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'isLoading': isLoading,
      'isAuth': isAuth,
      'appName': appName,
    };
  }

  factory App.fromMap(Map<String, dynamic> map) {
    return App(
      isLoading: map['isLoading'],
      isAuth: map['isAuth'],
      appName: map['appName'],
    );
  }

  String toJson() => json.encode(toMap());

  factory App.fromJson(String source) => App.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [isLoading, isAuth, appName];
}
