import 'dart:convert';

import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String userId;
  final String userName;
  final String userEmail;
  final String userPassword;
  final String userStatus;
  final String userNumber;
  User({
    this.userId,
    this.userName,
    this.userEmail,
    this.userPassword,
    this.userStatus,
    this.userNumber,
  });

  User copyWith({
    String userId,
    String userName,
    String userEmail,
    String userPassword,
    String userStatus,
    String userNumber,
  }) {
    return User(
      userId: userId ?? this.userId,
      userName: userName ?? this.userName,
      userEmail: userEmail ?? this.userEmail,
      userPassword: userPassword ?? this.userPassword,
      userStatus: userStatus ?? this.userStatus,
      userNumber: userNumber ?? this.userNumber,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'userName': userName,
      'userEmail': userEmail,
      'userPassword': userPassword,
      'userStatus': userStatus,
      'userNumber': userNumber,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      userId: map['userId'],
      userName: map['userName'],
      userEmail: map['userEmail'],
      userPassword: map['userPassword'],
      userStatus: map['userStatus'],
      userNumber: map['userNumber'],
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      userId,
      userName,
      userEmail,
      userPassword,
      userStatus,
      userNumber,
    ];
  }
}
